var username = "admin";
var password = '123';
var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
var notAllowNull = "Not allow null";
function toHomepage(){
    window.location.replace('https://www.w3schools.com/jquery/');
};

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

// Sign up
// ============================================================================
function signup(){
    $(document).ready(function(){
      $("#btnSignup").click(function(){
        alert("Click")
        var getusername = $.trim($("#username").val());
        var getpassword = $.trim($("#password").val());
        var getemail = $.trim($("#email").val());
        var getrepassword = $.trim($("#re-password").val());
  
        if(getusername==null || getusername=='' || getusername.length==0){
          //$("#usernameerror").text(notAllowNull);
          $("#username").attr('placeholder',notAllowNull);
          $("#username").focus();  
        }
        else{
            $("#username").attr('placeholder','Username');
        } 
  
        if(getemail==null || getemail=='' || getemail==0){
          $("#email").attr('placeholder',notAllowNull);
          $("#email").focus();
        }
        else{
          if(testEmail.test(getemail)){
            $("#email").attr('placeholder','Email');
          }
          else{
            $("#email").attr('placeholder','allow email type only');
            $("#email").focus();    
          }
        } 
  
        if(getpassword==null || getpassword=='' || getpassword.length==0){
          $("#password").attr('placeholder',notAllowNull);
          $("#password").focus();
        }
        else{
            $("#password").attr('placeholder','Password');
        } 
  
        if(getrepassword==null || getrepassword=='' || getrepassword.length==0){
          $("#re-password").attr('placeholder',notAllowNull);
          $("#re-password").focus();
        }
        else{
          $("#re-password").text("");
          if(getpassword!=getrepassword){
            alert("Password not match");
          }
          else{
            alert("Successful");
            setTimeout("toHomepage()",1000);
          }
        } 
        
      })
    })
  }
  signup();

// ============================================================================

// signin
// ===============================================================================
function signin(){
    $(document).ready(function(){
      $("#btn-login").click(function(){
        //alert("Click");
        var getusername = $.trim($("#login-username").val());
        var getpassword = $.trim($("#login-password").val());
        //alert(getusername);
        if(getusername==null || getusername=='' || getusername.length==0){
            $("#login-username").attr('placeholder',notAllowNull);
            $("#login-username").focus();
        }
        else if(getusername!=username){
            //$("#login-username").text("Wrong username");
        }
        else{
            $("#login-username").attr('placeholder','username or password')
        } 
        if(getpassword==null || getpassword=='' || getpassword.length==0){
            $("#login-password").attr('placeholder',notAllowNull);
            $("#login-password").focus();
        }
        else if(getpassword!=password){
          //$("#passworderror").text("Wrong password");
        }
        else{
            $("#login-password").attr('placeholder','password');
        }
        if(getusername==username && getpassword==password){
           alert("Successfully");
           setTimeout("toHomepage()",1000);
        }
        else{
          alert("Username or Password was wrong");
        }
      });
    });
  };
  signin();

// ===============================================================================
